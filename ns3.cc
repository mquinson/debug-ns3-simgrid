/*
 *
 * Four hosts three links topology
 *
 * +--------+             +--------+             +--------+             +--------+
 * |        |     10.1.0.1|        |     10.1.1.1|        |     10.1.2.1|        |
 * | Node 0 +-------------+ Node 1 +-------------+ Node 2 +-------------+ Node 3 |
 * |        |10.1.0.0     |        |10.1.1.0     |        |10.1.2.0     |        |
 * +--------+             +--------+             +--------+             +--------+
 *
 * Two hosts one link topology
 *
 * +--------+             +--------+
 * |        |     10.1.0.1|        |
 * | Node 0 +-------------+ Node 1 +
 * |        |10.1.0.0     |        |
 * +--------+             +--------+
 *
 *	DogBone: I let you guest :D
 *
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ecofen-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("microbenchmarks");

#define TRACE_LOCATION "./ns3trace.tr"
#define SIM_END 20 // WARNING !! If you change this, change the log parsing script where simulation time compute the energy consumption
#define PORT 123
#define ENERGY_LOG_RES 0.01

/**
 * Parse the CSV file
 */
std::vector<std::vector<std::string>> parseCSV(std::string filePath) {
	// For parsing CSV with SRC,DST (same exp on SG and NS)
	std::ifstream csvFile(filePath);
	std::vector < std::vector < std::string >> elements;
	bool header = true;
	if (csvFile.is_open()) {
		std::string line;
		while (std::getline(csvFile, line)) {
			std::vector<std::string> tokens;
			std::stringstream lineStream(line);
			std::string token;

			while (std::getline(lineStream, token, ',')) {
				tokens.push_back(token);
			}
			if (header) {
				header = false;
			} else
				elements.push_back(tokens);
		}

		csvFile.close();
	}
	else{
		NS_LOG_ERROR("Failed to open CSV file " <<filePath.c_str());
	}

	return (elements);
}

void assignCons(NodeContainer nodes, std::vector<std::vector<std::string>> energy){
	BasicNodeEnergyHelper basicNodeEnergy;
	ConsumptionLogger consoLogger;
	basicNodeEnergy.Set("OnConso", DoubleValue(0));
	basicNodeEnergy.Set("OffConso", DoubleValue(0));

	for(int i=0;i<nodes.GetN();i++){
		Ptr<Node> node=nodes.Get(i);
		std::vector<std::string> energyMatch;
		bool found=false;
		for (auto const& entry : energy) { // Find the entry of the node
			if(std::stod(entry.at(0))==i){
				energyMatch=entry;
				found=true;
			}
		}

		if(!found){
			NS_LOG_ERROR("Failed to found energy value for node "<<i<<" in energy csv file");
			exit(1);
		}

		double idleCons=std::stod(energyMatch.at(1));
		double byteCons=std::stod(energyMatch.at(2));
		NS_LOG_INFO("Setting node " << i << " energy consumption -> idle:"<<idleCons<<" byte:"<<byteCons);
		CompleteNetdeviceEnergyHelper completeNetdeviceEnergy;
		completeNetdeviceEnergy.Set("IdleConso", DoubleValue(idleCons));
		completeNetdeviceEnergy.Set("OffConso", DoubleValue(0));
		completeNetdeviceEnergy.Set("RecvByteEnergy", DoubleValue(byteCons));
		completeNetdeviceEnergy.Set("SentByteEnergy", DoubleValue(byteCons));
		completeNetdeviceEnergy.Set("RecvPktEnergy", DoubleValue(0));
		completeNetdeviceEnergy.Set("SentPktEnergy", DoubleValue(0));

		// Install
		//basicNodeEnergy.Install(node);
		//consoLogger.NodeConso(Seconds(ENERGY_LOG_RES), Seconds(SIM_END),node); // WARNING !! If you change this, change the log parsing script where simulation time compute the energy consumption
		//completeNetdeviceEnergy.Install(node);

	}
}

void initFlows(NodeContainer nodes, int flowType, int flowSize, int bw);

NodeContainer deducePlatform(short id, int bw, int delay, bool enableTrace) {
	NodeContainer txRx; // Contain |Tx1|Rx1|Tx2|Rx2|...

	std::ostringstream ss;
	ss << bw << "Mbps";
	StringValue bwS(ss.str());
	ss.str("");
	ss << delay << "ms";
	StringValue delayS(ss.str());

	switch (id) {
	case 1:
		NS_LOG_INFO("Creating 2 Hosts 1 Link platform...");
		{
			txRx.Create(2);	 // Create nodes
			InternetStackHelper stack;
			stack.Install(txRx);	 // Add protocol stack (TCP,UDP,IP)

			// Links & traces
			PointToPointHelper p2p;
			p2p.SetDeviceAttribute("DataRate", StringValue(bwS));
			p2p.SetChannelAttribute("Delay", StringValue(delayS));
			if (enableTrace) {
				ns3::AsciiTraceHelper ascii;
				p2p.EnableAsciiAll(ascii.CreateFileStream(TRACE_LOCATION));
			}
			NetDeviceContainer netDevTmp = p2p.Install(txRx);

			// Ips
			Ipv4AddressHelper IPv4;
			IPv4.SetBase("10.1.0.0", "255.255.255.0");
			IPv4.Assign(netDevTmp);

		}
		break;
	case 2:
		NS_LOG_INFO("Creating 4 Hosts 3 Links platform...");
		{
			NodeContainer nodes;
			InternetStackHelper stack;
			nodes.Create(4);	 // Create nodes
			stack.Install(nodes);	 // Add protocol stack (TCP,UDP,IP)

			for (int i = 0; i < 3; i++) // Configure links, ips and traces
					{
				NodeContainer tmp(nodes.Get(i), nodes.Get(i + 1));

				// Links & traces
				PointToPointHelper p2p;
				p2p.SetDeviceAttribute("DataRate", StringValue(bwS));
				p2p.SetChannelAttribute("Delay", StringValue(delayS));
				if (enableTrace) {
					ns3::AsciiTraceHelper ascii;
					p2p.EnableAsciiAll(ascii.CreateFileStream(TRACE_LOCATION));
				}
				NetDeviceContainer netDevTmp = p2p.Install(tmp);

				// Ips
				Ipv4AddressHelper IPv4;
				std::ostringstream ss;
				ss << "10.1." << i + 1 << ".0";
				IPv4.SetBase(ss.str().c_str(), "255.255.255.0");
				IPv4.Assign(netDevTmp);
			}
			txRx.Add(nodes.Get(0));
			txRx.Add(nodes.Get(3));


		}
		break;
	default:
		NS_LOG_INFO("Creating DogBone platform...");
		{
			NodeContainer p1, p2, p3, p4, p5;
			p1.Create(1);
			p2.Create(1);
			p3.Create(2);
			p4.Create(1);
			p5.Create(1);
			InternetStackHelper stack;
			stack.Install(NodeContainer(p1, p2, p3, p4, p5));

			// Botleneck
			p1.Add(p3.Get(0));
			p2.Add(p3.Get(0));
			p4.Add(p3.Get(1));
			p5.Add(p3.Get(1));

			// Links & traces
			PointToPointHelper p2p;
			p2p.SetDeviceAttribute("DataRate", StringValue(bwS));
			p2p.SetChannelAttribute("Delay", StringValue(delayS));
			if (enableTrace) {
				ns3::AsciiTraceHelper ascii;
				p2p.EnableAsciiAll(ascii.CreateFileStream(TRACE_LOCATION));
			}

			Ipv4AddressHelper IPv4;
			IPv4.SetBase("10.1.0.0", "255.255.255.0");
			IPv4.Assign(p2p.Install(p1));
			IPv4.SetBase("10.1.1.0", "255.255.255.0");
			IPv4.Assign(p2p.Install(p2));
			IPv4.SetBase("10.1.2.0", "255.255.255.0");
			IPv4.Assign(p2p.Install(p3));
			IPv4.SetBase("10.1.4.0", "255.255.255.0");
			IPv4.Assign(p2p.Install(p4));
			IPv4.SetBase("10.1.5.0", "255.255.255.0");
			IPv4.Assign(p2p.Install(p5));

			txRx.Add(p1.Get(0));
			txRx.Add(p4.Get(0));
			txRx.Add(p2.Get(0));
			txRx.Add(p5.Get(0));

		}

	}
	NS_LOG_INFO("Platform created !");
	return (txRx);
}

//--------------------------------------
int main(int argc, char *argv[]) {
	// Enable log info
	LogComponentEnable("microbenchmarks", LOG_LEVEL_INFO);
	LogComponentEnable("OnOffApplication", LOG_LEVEL_INFO);
	LogComponentEnable("PacketSink", LOG_LEVEL_INFO);

	Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (1040));
//	Config::SetDefault ("ns3::TcpSocket::DelAckCount", UintegerValue (1));


//	Config::SetDefault ("ns3::TcpSocketBase::Timestamp", BooleanValue (false));


	// Creating arguments
	int flowSize = 500;
	short flowType = 1;
	int bw = 80;
	short platform = 1;
	double latency=0;
	int pkt = 0;
	std::string csv("1-microBenchmarks/energy.csv");

	// Parsing arguments
	CommandLine cmd;
	cmd.AddValue("flowSize", "Number of byte sended by each hosts", flowSize);
	cmd.AddValue("latency", "Which latency on links (Mbps)",latency);
	cmd.AddValue("flowType", "Which flow configuration to use (from 1 to 4)",
			flowType);
	cmd.AddValue("bw", "The bandwidth of the link (Mbps)", bw);
	cmd.AddValue("platform", "1=2H1L, 2=4H3L and 3=DogBone", platform);
	cmd.AddValue("energyCSV", "File path to the energy CSV file", csv);
	cmd.AddValue("pkt", "Pkt energy consumption (nJ)", pkt);
	cmd.Parse(argc, argv);

	// Log INFO
	//NS_LOG_INFO("FlowSize:"<<flowSize<<" BW:"<<bw<<" Latency:"<<)

	// Define time resolution
	Time::SetResolution(Time::NS);

	// Init platforms ant flows configurations
	NodeContainer nodes = deducePlatform(platform, bw, latency,false);
	initFlows(nodes, flowType, flowSize,bw);

	// Enable routing !
	Ipv4GlobalRoutingHelper::PopulateRoutingTables();

	// Run simulation
	Simulator::Stop(Seconds(SIM_END));
	Simulator::Run();
	NS_LOG_INFO("Simulation end " << Simulator::Now().GetSeconds() << "s");
	Simulator::Destroy();

	return (0);
}

void initFlows(NodeContainer nodes, int flowType, int flowSize, int bw) {

	std::ostringstream ss;
	ss << bw << "Mbps";

	// Configure dest
	Address sinkLocalAddress(InetSocketAddress(Ipv4Address::GetAny(), PORT));
	PacketSinkHelper sinkHelper("ns3::TcpSocketFactory", sinkLocalAddress);

	// Configure src
	BulkSendHelper srcHelper("ns3::TcpSocketFactory", Address());
//	srcHelper.SetAttribute("OnTime",
//			StringValue("ns3::ConstantRandomVariable[Constant=1]"));
//	srcHelper.SetAttribute("OffTime",
//			StringValue("ns3::ConstantRandomVariable[Constant=0]"));
	srcHelper.SetAttribute("MaxBytes", UintegerValue(flowSize));
	srcHelper.SetAttribute("SendSize", UintegerValue(1040));

	//srcHelper.SetAttribute("DataRate", DataRateValue(DataRate(ss.str())));
	srcHelper.SetAttribute("Protocol",
			TypeIdValue(TcpSocketFactory::GetTypeId()));

	ApplicationContainer apps;

	// Assign flows
	if (flowType == 1) {
		for (uint32_t i = 0; i < nodes.GetN(); i++) {
			if (i % 2 == 0) { // We are on a srx (tx)
				// Pickup rx ip address
				Ptr < Ipv4 > ipv4 = nodes.Get(i + 1)->GetObject<Ipv4>();
				Ipv4InterfaceAddress iaddr = ipv4->GetAddress(1, 0);
				Ipv4Address ipAddr = iaddr.GetLocal();
				AddressValue remoteAddress(InetSocketAddress(ipAddr, PORT));
				srcHelper.SetAttribute("Remote", remoteAddress);
				apps.Add(srcHelper.Install(nodes.Get(i)));
			} else { // We are on a dst (rx)
				apps.Add(sinkHelper.Install(nodes.Get(i)));
			}
		}
	} else if (flowType == 2) {
		for (uint32_t i = 0; i < nodes.GetN(); i++) {
			if (i % 2 == 0) { // We are on a srx (tx)
				// Pickup rx ip address
				Ptr < Ipv4 > ipv4 = nodes.Get(i + 1)->GetObject<Ipv4>();
				Ipv4InterfaceAddress iaddr = ipv4->GetAddress(1, 0);
				Ipv4Address ipAddr = iaddr.GetLocal();
				AddressValue remoteAddress(InetSocketAddress(ipAddr, PORT));
				srcHelper.SetAttribute("Remote", remoteAddress);
				apps.Add(srcHelper.Install(nodes.Get(i))); // Two on the same way
				apps.Add(srcHelper.Install(nodes.Get(i))); // Two on the same way

			} else { // We are on a dst (rx)
				apps.Add(sinkHelper.Install(nodes.Get(i)));
			}
		}
	} else if (flowType == 3) {
		for (uint32_t i = 0; i < nodes.GetN(); i++) {
			if (i % 2 == 0) { // We are on a srx (tx)
				// Pickup rx ip address
				Ptr < Ipv4 > ipv4 = nodes.Get(i + 1)->GetObject<Ipv4>();
				Ipv4InterfaceAddress iaddr = ipv4->GetAddress(1, 0);
				Ipv4Address ipAddr = iaddr.GetLocal();
				AddressValue remoteAddress(InetSocketAddress(ipAddr, PORT));
				srcHelper.SetAttribute("Remote", remoteAddress);
				apps.Add(srcHelper.Install(nodes.Get(i)));

				apps.Add(sinkHelper.Install(nodes.Get(i))); // Rx also

			} else { // We are on a dst (rx)
				apps.Add(sinkHelper.Install(nodes.Get(i)));

				// Tx also
				Ptr < Ipv4 > ipv4 = nodes.Get(i - 1)->GetObject<Ipv4>();
				Ipv4InterfaceAddress iaddr = ipv4->GetAddress(1, 0);
				Ipv4Address ipAddr = iaddr.GetLocal();
				AddressValue remoteAddress(InetSocketAddress(ipAddr, PORT));
				srcHelper.SetAttribute("Remote", remoteAddress);
				apps.Add(srcHelper.Install(nodes.Get(i)));
			}
		}
	} else if (flowType == 4) {
		for (uint32_t i = 0; i < nodes.GetN(); i++) {
			if (i % 2 == 0) { // We are on a srx (tx)
				// Pickup rx ip address
				Ptr < Ipv4 > ipv4 = nodes.Get(i + 1)->GetObject<Ipv4>();
				Ipv4InterfaceAddress iaddr = ipv4->GetAddress(1, 0);
				Ipv4Address ipAddr = iaddr.GetLocal();
				AddressValue remoteAddress(InetSocketAddress(ipAddr, PORT));
				srcHelper.SetAttribute("Remote", remoteAddress);
				apps.Add(srcHelper.Install(nodes.Get(i))); // Two on the same way
				apps.Add(srcHelper.Install(nodes.Get(i))); // Two on the same way

				apps.Add(sinkHelper.Install(nodes.Get(i))); // Rx also

			} else { // We are on a dst (rx)
				apps.Add(sinkHelper.Install(nodes.Get(i)));

				// Tx also
				Ptr < Ipv4 > ipv4 = nodes.Get(i - 1)->GetObject<Ipv4>();
				Ipv4InterfaceAddress iaddr = ipv4->GetAddress(1, 0);
				Ipv4Address ipAddr = iaddr.GetLocal();
				AddressValue remoteAddress(InetSocketAddress(ipAddr, PORT));
				srcHelper.SetAttribute("Remote", remoteAddress);
				apps.Add(srcHelper.Install(nodes.Get(i)));
			}
		}
	}

	apps.Start(Seconds (0.0));
	apps.Stop(Seconds (SIM_END));
}

