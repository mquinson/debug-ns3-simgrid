/* Copyright (c) 2017-2018. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include "simgrid/s4u/VirtualMachine.hpp"
#include "simgrid/plugins/energy.h"
#include "xbt/log.h"
#include <simgrid/s4u.hpp>

#include <iostream>
#include <string>
#include <random>
#include <simgrid/msg.h>

#include "ns3/core-module.h"
#include "ns3/consumption-logger.h"
#include "ns3/nstime.h"
#include "ns3/complete-netdevice-energy-helper.h" /* ecofen */
#include "ns3/basic-node-energy-helper.h" /* ecofen */
#include "ns3/ecofen-module.h"
#include "ns3/linear-netdevice-energy-helper.h"
#include "src/surf/ns3/ns3_simulator.hpp" /* internal header! */
#include "simgrid/kernel/routing/NetPoint.hpp" /* internal header! */

XBT_LOG_NEW_DEFAULT_CATEGORY(simulator, "Simulator logging channel");

/**
 * Helper function for instanciate the whole simulation
 */
void buildHosts(std::string flowScenario, std::string nbLoop,
		std::string dataSize);

/**
 * Define the beahavior of a flow actor
 */
static void flowActor(std::vector<std::string> args);

/**
 * Helper function to create a flow actor for a given host
 */
static void buildFlowActor(std::string hostName, std::string nbLoop,
		std::string dataSize, bool isSender, std::string dstHostName,
		int senderFlowId);

/**
 * Entry point
 */
int main(int argc, char **argv) {
	// Try to find if we use NS3 for the execution
	bool use_ns3 = false;
	for (int i = 0; i < argc; i++) {
		std::string s(argv[i]);
		if (s.find("NS3") != std::string::npos) {
			use_ns3 = true;
		}
	}

	// Build engine
	simgrid::s4u::Engine engine(&argc, argv);

	// If we dont use NS3 use energy plugin
	if (!use_ns3)
		sg_link_energy_plugin_init();

	// Make sure that we pass the right parameters to ecofen for each host when we use NS3
	if (use_ns3) {
		simgrid::s4u::on_platform_created.connect(
				[argv]()
				{

					sg_host_t* host_list = sg_host_list();
					int host_count = sg_host_count();
					for (int i = 0; i < host_count; i++)
					{
						auto host = host_list[i];
						if (dynamic_cast<simgrid::s4u::VirtualMachine*>(host) == nullptr)
						{  // Ignore virtual machines

							// List properties to fetch from platform file
							const char *properties[] =
							{
								"IdleConso", "OffConso",
								"RecvByteEnergy", "SentByteEnergy",
								"RecvPktEnergy", "SentPktEnergy"
							};

							bool found = false;
							ns3::CompleteNetdeviceEnergyHelper completeNetdeviceEnergy;

							// Fetch and assign parameters to completeNetdeviceEnergy
							for (const char* prop : properties)
							{

								const char* value = host->get_property(prop);
								if (value != nullptr)
								{
									found = true;
									try
									{
										double dval = std::stod(value);
										completeNetdeviceEnergy.Set (prop, ns3::DoubleValue (dval));

									}
									catch (std::invalid_argument& ia)
									{
										throw std::invalid_argument(std::string("Value of property ")+prop+" for host "+host->get_name()+" is not numerical:" + ia.what());
									}
								}
							}

							if (found)
							{
								// Fetch the node
								ns3::Ptr<ns3::Node> node=host->pimpl_netpoint->extension<NetPointNs3>()->ns3_node_;

								// Apply node energy consumption
								ns3::BasicNodeEnergyHelper basicNodeEnergy;
								basicNodeEnergy.Set("OnConso", ns3::DoubleValue (0.0));
								basicNodeEnergy.Set("OffConso", ns3::DoubleValue (0.0));
								basicNodeEnergy.Install (node);

								// Apply NIC energy consumption
//								CompleteNetdeviceEnergyHelper completeNetdeviceEnergy;
//								completeNetdeviceEnergy.Set ("IdleConso", DoubleValue(1.0));
//								completeNetdeviceEnergy.Set ("OffConso", DoubleValue(0.2));
//								completeNetdeviceEnergy.Set ("RecvByteEnergy", DoubleValue (100.0));
//								completeNetdeviceEnergy.Set ("SentByteEnergy", DoubleValue (100.0));
//								completeNetdeviceEnergy.Set ("RecvPktEnergy", DoubleValue (300.0));
//								completeNetdeviceEnergy.Set ("SentPktEnergy", DoubleValue (300.0));
								completeNetdeviceEnergy.Install (node);

								// Enable logging
								ns3::ConsumptionLogger conso;
								conso.NodeConso(ns3::Seconds (0.01), ns3::Seconds(1000.0), node);

								// Assign MTU
								int nDev=node->GetNDevices();
								for(int i=0;i<nDev;i++){
									node->GetDevice(i)->SetMtu(std::stoi(argv[4]));
								}

							}
						}
					}
				});
	}

	// Check parameters
	xbt_assert(argc == 5,
			"Usage : %s <platform> <flowScenario> <loop> <dataSize> [--cfg=network/model:NS3]",
			argv[0]);

	// Load platform and parameters and build actors
	engine.load_platform(argv[1]);
	std::string flowScenario(argv[2]), nbLoop(argv[3]), dataSize(argv[4]);
	buildHosts(flowScenario, nbLoop, dataSize);

	// Run simulation
	XBT_INFO("----- Simulation start -----");
	engine.run();
	XBT_INFO("----- Simulation end -----");

	return (0);
}

/**
 * Correctly build the hosts according to the flowScenario
 */
void buildHosts(std::string flowScenario, std::string nbLoop,
		std::string dataSize) {
	// Load and check flowScenario parameter
	short flowScenarioNb = std::stoi(flowScenario);
	xbt_assert(flowScenarioNb >= 1 && flowScenarioNb <= 5,
			"Invalid flow scenario %d, please choose another.", flowScenarioNb);

	XBT_INFO("Init hosts using flow scenario %d", flowScenarioNb);
	if (sg_host_count() != 6) { 	// Check if we are not on DogBone platform (6 hosts)
		// Build flow according to scenario 1 to 4
		switch (flowScenarioNb) {
		case 1:
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host2", 1); // One send flow
			buildFlowActor("Host2", nbLoop, dataSize, false, "Host1", 0); // One receive flow
			break;

		case 2:
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host2", 1); // First send flow
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host2", 2); // Second send flow

			buildFlowActor("Host2", nbLoop, dataSize, false, "Host1", 0); // First receive flow
			buildFlowActor("Host2", nbLoop, dataSize, false, "Host1", 0); // Second receive flow
			break;

		case 3:
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host2", 1); // One send flow
			buildFlowActor("Host1", nbLoop, dataSize, false, "Host2", 0); // One receive flow

			buildFlowActor("Host2", nbLoop, dataSize, true, "Host1", 2); // One send flow
			buildFlowActor("Host2", nbLoop, dataSize, false, "Host1", 0); // One receive flow
			break;

		default:
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host2", 1); // First send flow
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host2", 2); // Second send flow
			buildFlowActor("Host1", nbLoop, dataSize, false, "Host2", 0); // One receive flow

			buildFlowActor("Host2", nbLoop, dataSize, true, "Host1", 3); // One send flow
			buildFlowActor("Host2", nbLoop, dataSize, false, "Host1", 0); // First receive flow
			buildFlowActor("Host2", nbLoop, dataSize, false, "Host1", 0); // Second receive flow
		}
	} else // Build scenario for Dogbone
	{
		// Build flow according to scenario 1 to 4
		switch (flowScenarioNb)
		{
		case 1:
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host3", 1); // One send flow
			buildFlowActor("Host3", nbLoop, dataSize, false, "Host1", 0); // One receive flow

			buildFlowActor("Host2", nbLoop, dataSize, true, "Host4", 2); // One send flow
			buildFlowActor("Host4", nbLoop, dataSize, false, "Host2", 0); // One receive flow
			break;

		case 2:
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host3", 1); // First send flow
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host3", 2); // Second send flow
			buildFlowActor("Host3", nbLoop, dataSize, false, "Host1", 0); // First send flow
			buildFlowActor("Host3", nbLoop, dataSize, false, "Host1", 0); // Second send flow

			buildFlowActor("Host2", nbLoop, dataSize, true, "Host4", 1); // First send flow
			buildFlowActor("Host2", nbLoop, dataSize, true, "Host4", 2); // Second send flow
			buildFlowActor("Host4", nbLoop, dataSize, false, "Host2", 0); // First send flow
			buildFlowActor("Host4", nbLoop, dataSize, false, "Host2", 0); // Second send flow
			break;

		case 3:
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host3", 1); // First send flow
			buildFlowActor("Host3", nbLoop, dataSize, false, "Host1", 0); // First send flow
			buildFlowActor("Host3", nbLoop, dataSize, true, "Host1", 2); // Second send flow
			buildFlowActor("Host1", nbLoop, dataSize, false, "Host3", 0); // Second send flow

			buildFlowActor("Host2", nbLoop, dataSize, true, "Host4", 1); // First send flow
			buildFlowActor("Host4", nbLoop, dataSize, false, "Host2", 0); // First send flow
			buildFlowActor("Host4", nbLoop, dataSize, true, "Host2", 2); // Second send flow
			buildFlowActor("Host2", nbLoop, dataSize, false, "Host4", 0); // Second send flow
			break;

		default:
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host3", 1); // First send flow
			buildFlowActor("Host3", nbLoop, dataSize, false, "Host1", 0); // First send flow
			buildFlowActor("Host1", nbLoop, dataSize, true, "Host3", 2); // First send flow
			buildFlowActor("Host3", nbLoop, dataSize, false, "Host1", 0); // First send flow
			buildFlowActor("Host3", nbLoop, dataSize, true, "Host1", 3); // Second send flow
			buildFlowActor("Host1", nbLoop, dataSize, false, "Host3", 0); // Second send flow

			buildFlowActor("Host2", nbLoop, dataSize, true, "Host4", 1); // First send flow
			buildFlowActor("Host4", nbLoop, dataSize, false, "Host2", 0); // First send flow
			buildFlowActor("Host2", nbLoop, dataSize, true, "Host4", 1); // First send flow
			buildFlowActor("Host4", nbLoop, dataSize, false, "Host2", 0); // First send flow
			buildFlowActor("Host4", nbLoop, dataSize, true, "Host2", 2); // Second send flow
			buildFlowActor("Host2", nbLoop, dataSize, false, "Host4", 0); // Second send flow
		}



	}
}

static void buildFlowActor(std::string hostName, std::string nbLoop,
		std::string dataSize, bool isSender, std::string dstHostName,
		int senderFlowId) {
	// Init actor arguments
	std::vector < std::string > args;
	args.push_back(nbLoop);
	args.push_back(dataSize);
	args.push_back(std::to_string(isSender));
	args.push_back(dstHostName);
	args.push_back(std::to_string(senderFlowId));

	// Build actor
	simgrid::s4u::Actor::create(hostName.c_str(),
			simgrid::s4u::Host::by_name(hostName), flowActor, args);
}

static void flowActor(std::vector<std::string> args) {
	// Load arguments from args and init variables
	int loop = std::stoi(args.at(0));
	unsigned long dataSize = std::stol(args.at(1));
	bool isSender = std::stoi(args.at(2));
	std::string dstName = args.at(3);
	int senderFlowId = std::stoi(args.at(4));

	// Define mailboxes
	std::string selfName = simgrid::s4u::this_actor::get_host()->get_name();
	simgrid::s4u::MailboxPtr dstMailbox = simgrid::s4u::Mailbox::by_name(
			dstName);
	simgrid::s4u::MailboxPtr selfMailbox = simgrid::s4u::Mailbox::by_name(
			simgrid::s4u::this_actor::get_host()->get_name());

	// Define flow actor behavior
	for(int i=0;i<loop;i++){
		if (isSender) {
			double comStartTime = simgrid::s4u::Engine::get_clock();
			dstMailbox->put(const_cast<char *>("message"), dataSize);
			double comEndTime = simgrid::s4u::Engine::get_clock();
			XBT_INFO(
					"%s sent %d bytes to %s in %f seconds from %f to %f with flow id %d",
					selfName.c_str(), dataSize, dstName.c_str(),
					comEndTime - comStartTime, comStartTime, comEndTime,
					senderFlowId);
		} else {
			selfMailbox->get();
		}
	}

}
