﻿## Debugging Current NS3-SimGrid Bindings
### Installation
1. Install NS3 with ECOFEN wherever you want
2. Install Simgrid with NS3 binding wherever you want
3. Clone this repo:

  git clone git@framagit.org:mquinson/debug-ns3-simgrid.git

3. Update the different Makefile variables according to your NS3 and Simgrid installation location: 

  NS3_LIBS_LOCATION,NS3_INCLUDES_LOCATION,
  NS3_VERSION,SG_LIBS_LOCATION,SG_INCLUDES_LOCATION

4. Then run make to compile the source code

### First Problem
Native Simgrid and native NS3 give same simulation times. You can verify it by running the two following command:

                 #                          # Lat 10ms # Lat 0ms
  make run-sg    # model CM02               #  0.62s   #  0.59s
                 # default model, 80Mbps    #  0.95s   #
                 # CM02, gamma = 131072     #  2.43s   #  0.59s
  make run-ns3   # raw-NS3 BulkTransfer     #  3.16s   #  0.57s
                 # raw-NS3 OnOffApplication #  0.93s	
  make run-sgns3 # SG+NS3 (73.6Mbps)        #  2.89s   #  0.59s
                 # SG+NS3 (80Mbps)          #          #  0.54s

However, when using ns3 bindings of Simgrid, we have a simulation time totally different. You can verify it by running:

Used platform: 4 hosts, 3 links. 
  - Each link in SG: bandwidth="73.6Mbps" latency="10ms" # 80Mbps *.92 to account for TCP headers
  - Each link in NS3: --bw=80 Mbps, delay="10ms"

Data sent: 5MiB (5242880 octets)

### Second Problem
When using ns3 bindings of Simgrid we can have infinite loop for some flow size. You can verify it by running:

> make run-bug # Should never end

