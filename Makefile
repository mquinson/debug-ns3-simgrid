NS3_LIBS_LOCATION=/opt/ecofen/lib64
NS3_INCLUDES_LOCATION=/opt/ecofen/include
NS3_VERSION=3.27

SG_LIBS_LOCATION=/home/loic/Documents/Git/mquinson/simgrid/lib
SG_INCLUDES_LOCATION=/home/loic/Documents/Git/mquinson/simgrid/include


all: ns3 simgrid

ns3: ns3.cc
	g++ -DNS3_LOG_ENABLE $^ -o $@ -I $(NS3_INCLUDES_LOCATION)/ns$(NS3_VERSION)/ -L $(NS3_LIBS_LOCATION)\
	  -lns$(NS3_VERSION)-core-debug -lns$(NS3_VERSION)-point-to-point-debug -lns$(NS3_VERSION)-network-debug -lns$(NS3_VERSION)-csma-debug -lns$(NS3_VERSION)-internet-debug -lns$(NS3_VERSION)-applications-debug \
	  -lns$(NS3_VERSION)-mpi-debug -lns$(NS3_VERSION)-stats-debug -lns$(NS3_VERSION)-bridge-debug -lns$(NS3_VERSION)-traffic-control-debug -lns$(NS3_VERSION)-config-store-debug \
	  -lns$(NS3_VERSION)-ecofen-debug

simgrid: simgrid.cc
	g++ -std=gnu++11 $^ -o  $@ -I $(SG_INCLUDES_LOCATION) -I$(NS3_INCLUDES_LOCATION)/ns$(NS3_VERSION)/  -L$(SG_LIBS_LOCATION) -lsimgrid -I $(dir $(SG_INCLUDES_LOCATION))\
	  -L $(NS3_LIBS_LOCATION)\
	  -lns$(NS3_VERSION)-core-debug -lns$(NS3_VERSION)-point-to-point-debug -lns$(NS3_VERSION)-network-debug -lns$(NS3_VERSION)-csma-debug -lns$(NS3_VERSION)-internet-debug -lns$(NS3_VERSION)-applications-debug \
	  -lns$(NS3_VERSION)-mpi-debug -lns$(NS3_VERSION)-stats-debug -lns$(NS3_VERSION)-bridge-debug -lns$(NS3_VERSION)-traffic-control-debug -lns$(NS3_VERSION)-config-store-debug \
	  -lns$(NS3_VERSION)-ecofen-debug

run-sgns3:  simgrid
	@echo "XXXX SimGrid RUN using NS3 model"
	LD_LIBRARY_PATH=$(SG_LIBS_LOCATION):$(NS3_LIBS_LOCATION) ./$^ platform.xml 1 1 5242880 --cfg=network/model:NS3 --log=ns3.t:debug
	@echo "On avait dit qu'on voulait de l'ordre de 0.93s."

run-sg:  simgrid
	@echo "XXXX SimGrid RUN using CM02 model"
	LD_LIBRARY_PATH=$(SG_LIBS_LOCATION):$(NS3_LIBS_LOCATION) ./$^ platform.xml 1 1 5242880 --cfg=network/model:CM02 --log=ns3.t:debug
	@echo "On avait dit qu'on voulait de l'ordre de 0.93s."

run-ns3: ns3
	@echo "XXXX NS3 Raw RUN"
	LD_LIBRARY_PATH=$(NS3_LIBS_LOCATION) ./$^ --platform=2 --latency=10 --bw=80 --flowSize=5242880 

run-bug: simgrid
	@echo "This bug should highlight infinite loop bug of NS3 model on simgrid"
	LD_LIBRARY_PATH=$(SG_LIBS_LOCATION):$(NS3_LIBS_LOCATION) ./$^ platform.xml 1 1 22 --cfg=network/model:NS3 --log=ns3.t:debug

clean: 
	rm -f ns3 simgrid

.PHONY: clean run-sgns3 run-sg run-ns3 run-bug
